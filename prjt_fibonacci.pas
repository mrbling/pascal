PROGRAM prjt_fibonacci;// Anthony Khan
uses crt;

VAR o,r1: integer;

PROCEDURE inf; // mostrar informacion acerca de fibonacci
begin
clrscr;
	writeln(' FIBONACCI');
	writeln;
	writeln('En matematicas, la sucesion o serie de Fibonacci hace referencia a la secuencia ordenada de numeros descrita por Leonardo de Pisa, matematico italiano del siglo XIII: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144,… A cada uno de los elementos de la serie se le conoce con el nombre de numero de Fibonacci.');
end;

PROCEDURE calculo; // mostrar la serie de numeros hasta el numero ingresado
VAR a,b,c,r:longint;
begin
clrscr;
writeln(' Ingrese el numero a buscar: ');
readln(r);
writeln;
clrscr;
    
a:=0;
b:=1;
c:=a+b;

if r=1 then
	writeln(a);
if r=2 then
	writeln(a,' ',b);
if r=3 then
	writeln(a,' ',b,' ',c);
if r>3 then
	writeln(' "Su numero sera resaltado en color rojo"'); // 55, 6765, 144
	writeln;
	write(a,' ',b,' ',c);
begin
 repeat
 if c<r then // si el numero ingresado por el usuario es menor al ultimo valor de la serie, entonces se mantendra el bucle
	begin
	a:=b;
	b:=c;
	c:=a+b;
	if r=c then // si el numer ingresado por el usuario es igual al ultimo valor del bucle, significa que el numero ingresado se encuentra en la serie, y sera resaltado en rojo
		begin
		textcolor(red);
		write(' ',c);  
		end
 else
	begin
	write (' ',c);
	end;
 end
 until (c=r) or (c>r);
	if c>r then // si el ultimo valor del bucle es mayor al numero ingresado, significa que el numero ingresado no existe en la serie
		begin
		clrscr;
		writeln(' El numero ingresado no se encuentra en la secuencia fibonacci');
		end		
end; 
end;

PROCEDURE imp; // mostrar el valor de la posicion ingresada 
VAR a,b,c,r,cont:longint;
begin
clrscr;
writeln(' Ingrese la posicion para conocer su valor: ');
readln(r);
writeln;
clrscr;
    
a:=0;
b:=1;
c:=a+b;
cont:=3;

if r=1 then
	writeln(' El valor en la posicion ',r,' es: ',a);
if r=2 then
	writeln(' El valor en la posicion ',r,' es: ',b);
if r=3 then
	writeln(' El valor en la posicion ',r,' es: ',c);
if r>3 then
begin
 repeat
	cont:=cont+1; // repetira el bucle hasta que el contador sea igual al valor ingresado, mostrando el ultimo valor de la posicion
	a:=b;
	b:=c;
	c:=a+b;
 until cont=r;
 writeln(' El valor en la posicion ',r,' es: ',c);
end; 
end;

PROCEDURE salir; // salir del sistema
begin
clrscr;
writeln(' Gracias por usar el sistema');
end;

BEGIN
	repeat
	writeln('**BIENVENIDO**');
	writeln;
	writeln(' Seleccione una opcion:');
	writeln;
	writeln('1: Informacion sobre la sucesion de fibonacci');
	writeln('2: Calculo de posicion');
	writeln('3: Impresion de la sucesion de fibonacci');
	writeln('4: Salir');
	readln(o);
	
	case o of
		1: inf;
		2: calculo;
		3: imp;	
		4: salir;
	end;	
	
readkey;	
clrscr;
	textcolor(white);
	writeln(' Desea volver al menu?');
	writeln;
	writeln(' "1" para volver');
	writeln(' "2" para salir');
	readln(r1);
	clrscr;
	until r1>1; 
	salir;
END.

